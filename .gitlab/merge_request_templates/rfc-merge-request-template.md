# Note to RFC reviewers

This merge request description is not the actual RFC. To view the actual RFC, click **Changes** in the top menu for this merge request.

__NOTE__: We recommend reading the files in their rendered format rather than their raw format.


## Current RFC status

- [x] Under discussion (until YYYY-MM-DD) {Set tentative date for 2 weeks after opening this pull request.}
- [ ] Final comment and voting (until YYYY-MM-DD) {Set date for one week after it enters this stage.}
- [ ] Accepted
- [ ] Rejected
- [ ] Implemented
- [ ] Deferred
- [ ] Withdrawn
- [ ] On hold or blocked


## Current review status

__NOTE__: Put an X in column if you've reviewed or voted on the RFC.

| Reviewed | Voted | Reviewer |
| -------- | ----- | -------- | 
|          |       | Aaron Peters @acpkendo |
|          |       | Alyssa Rock @barbaricyawps |
|          |       | Ankita Tripathi  |
|          |       | Bryan Klein @bwklein |
|          |       | Cameron Shorter @camerons1 |
|          |       | Carrie Crowe @Carrie1138 |
|          |       | Deanna Thompson @itsdeannat |
|          |       | Erin McKean @emckean |
|          |       | Felicity Brand @flicstar |
|          |       | Gayathri Krishnaswamy @gayathri-krishnaswamy |
|          |       | Morgan Craft @mgan59 |
|          |       | Nelson Guya @nelsonkimani63 |
|          |       | Ryan Macklin @ryanmacklin   |
|          |       | Tina Lüdtke @kickoke  |
