# RFC-014: Product vision for "templates 1.0" (2022-23) [WITHDRAWN]

## Proposed by

Ryan Macklin ([@macklin](https://thegooddocs.slack.com/team/U01DYRWG43X))

Initially submitted on 7 Sep 2022

Withdrawn on 31 Jan 2023, is outdated after seeing Alyssa's product framework presentation

* There may be useful info in this RFC for future product vision discussion

## Current status

- [ ] Draft
- [ ] Under discussion
- [ ] Final comment and voting (until YYYY-MM-DD) {{Add date after selecting this status.}}
- [ ] Accepted
- [ ] Rejected
- [ ] Implemented
- [ ] Deferred
- [x] Withdrawn on 13 Jan 2023

## Proposal overview

Our core mission and focus—creating templates—needs a vision to guide it, so we can achieve a genuine "1.0" release we can as a group be proud of and excited to be a part of. This is a framework for that product vision. This RFC focuses on a shift of how to think about and work toward our ambitious and sizable goal.

This proposal has us releasing "templates 1.0" in late 2023, with a 0.9 shortly before WTD Portland 2023.

### Side note about the word "product"

Some people don't like the word "product," but broadly speaking, we're word nerds here. We're aiming to *produce* a set of usable templates, and our templates are set up as milestone-informed packages. That's a product—not commercially, but essentially. So we'll use that term to separate user-facing deliverables like templates (and later doc tools) from non-products like the blog, news, etc.

## Motivation

As an organization, we've been talking about having template releases for years. We've been actively recruiting people to help participate in the process, but we've lacked a critical element: an overarching plan for what's a "1.0" release of our templates.

On a logistics level, it feels hard to pitch our project to people, as we've spent multiple WTD Portlands talking about goals and aspirations, but not providing someone concrete that proves The Good Docs Project isn't just vaporware. As an organization, we could use the credibility of achieving a concrete 1.0 launch.

And if we're aiming to support devs who are overwhelmed or scared to make docs, they could use that 1.0 launch too.

Another very important benefit of having a product plan/roadmap is that it'll help us better identify what template work needs to be done (thus filling in our backlog and issue list). We have a perpetual problem of new community members who join our project and are excited to create templates, but very few of them have their own independent ideas for what templates they could work on. Many templateers would prefer for us to say what work needs to be done and what our priorities are. Much of the time, we just make this up and talk it out on an ad-hoc basis, but it would be great to have a clear plan that clearly indicates what work needs to be done.

So basically, a product plan/roadmap will supercharge our productivity and improve our onboarding process for new templateers, reducing the burden on template leads to invent work for new community members to do on an ad-hoc basis.

## Proposal

Simply put: It's time to decide as a group what *should* be in "Templates 1.0." Rather than working solely bottom-up, with people deciding what templates inspire them or working on other projects, we should add *some* top-down direction to shape the work.

This includes (as post-RFC decisions):
 * Defining our definition of "done & confidently releasable" for individual templates
 * Deciding what meets our quality standards and acceptance criteria

### What makes up the template product 1.0

- Whatever templates we deem as "core" (which is a follow-up conversation)
- Whatever other templates happen to be done, that feel cohesive rather than oddly placed
	- Those that feel out of place can continue to exist in a dev channel, until they feel like they're part of a whole (if any such templates fall under this category)
- Basic supplemental material on how to use the templates
- Basic Chronologue execution of templates

#### What doesn't have to be in 1.0

These are things that likely many of us would want to have as part of any big release, but need to not block a release:

- Detailed best practices
- Templates with a smaller use case, that happens to be someone's passion
- Material related to docs but don't directly serve the "open source dev scared of writing docs," like how to sell the value of docs in one's organization
- In-depth Chronologue execution

These are all great things! But if we try to fit every great thing into a 1.0, we'll never release. That said, we have a release valve for some of these: publishing them as blog posts (and possibly later compiling or basing material off of said posts).

Additionally, one thing floated about in broader conversations is creating additional types of content that'd follow the same contributing process as the template, but would be hosted in a different part of the project. We've talked about creating guides and we've talked about curriculum. We could call those out specifically as not being part of 1.0: guides and curriculum.

### Who's involved in deciding what's 1.0

In short, everyone but with a heavy asterisk. 

The product leader (see below) would drive the conversation, and have a stake somewhere between "minimum viable" and "robust." Fully robust asks too much of volunteers on a finine timeframe, and MVP may be too minor to be of use to our userbase.

The template leads would brainstorm, collaborate, and sign off on what is 1.0. No need for a separate RFC for that, though the conversation would be open to the entire project, not closed.

Template contributors should get to weigh in and help craft as well, though not to the point where the scope of 1.0 spirals into something that isn't managable. To that point: it's likely that elements may be deemed to be "post 1.0" or "let's get part of that in 1.0, and put the rest on our post-1.0 roadmap."

### "Why is what's 'core' a follow-up conversation?"

This sort of working is a big shift in our working models, so before we get into specifics, we should come to an accord on this overall.

This includes discussions about what framework the product takes, such as the "template packs" discussions.

### Ideal timeline

Given our tethering to the Write the Docs calendar, here's a proposal:

 - We have a "0.9 release candidate" ready and out 2 weeks prior to WTD Portland
	 - That requires some of the content strategy work done regarding users being able to navigate our site
	 - We'll need to provide easy and clear ways for people to submit feedback to us
 - We have a 1.0 release ready and out 2 weeks prior to WTD Prague

 That said, if we can't hit this timeline, but the rest of this RFC looks legit to the steering committee, this element is deemed as flexible. (Similar to deciding what is/isn't 1.0.) However, the author strongly wants to assert a timeframe, as it's more beneficial to work toward a timeframe and fall short than it is to work without a timeframe.

### "Wait, a 0.9 release candidate?"

Yep. If we can have something that feels mostly complete, from a 1.0 perspective, but could either use testing or some more work, we can make that our 0.9. And if we have that 0.9 ready shortly before WTD Portland, we can present that at Writing Day and in Unconferences to:

* Demonstrate concrete progress with a sense of "full release upcoming" we can share with the wider community
* Better reach devs looking for docs help, by having a platform to speak both to devs and to writer friends such devs may have

### Having a product leader role

Part of what would help us forge a path toward a 1.0 is having someone keeping an eye of what as a group we're working on, and how those efforts can intersect with the product vision… or force the vision to adapt to reality. That would be a **product leader**.

Ryan is proposing (in third person) of taking on that role, at least until we get to a 1.0 release. He doesn't have an attachment to any given template working group, so he's hoping that having a less biased viewpoint could help in offering product direction. (If anyone else wants to be a part of this role, that's welcomed! Though we'd like the tempalte leads to stick to production, and not split their focus.) 

The product leader would be in continual contact with the template leads, to get information from them regarding the template pipeline, and to offer guidance that shapes work toward the agreed upon 1.0.

Once we have a 1.0 release, we may or may not need a product leader role. We'll figure that out when we get there. This RFC's scope just gets us to 1.0.

(*Clarification should it be needed:* This role isn't a project manager. Project manager roles involve looking at resources and trying to keep tasks and larger initiatives moving. Product leaders work to make choices about what tasks and initiatives suit the product best. In some orgs, the same person handles both.)

### Managing points of failure (a.k.a. respecting our contributor community)

Because we're a volunteer organization, it can be hard to pin down long-term objectives versus the resources that are able to genuinely work on them. We might end up deciding that some given template or deliverable is "core," only for that contributor to be unable to complete it. That's understandable!

This product vision needs to understand that we'll have some failure points, and that we'll have to make compromises and shifts in the overall plan as we that happens. Part of the product management role will be to try to keep an eye on those various tasks or initiatives, to see if any failure points can be mitigated early on. And when they can't be, then we adapt and do the best we can. That might mean pushing some timelines back, or reducing the overall list of deliverables, or seeing if others can shift their own energies and help with a sudden hole in production.

This is all to say: just because we draft a vision for 1.0, that doesn't give us an excuse to be inflexible with respect to the needs of our community members. One of the things I mentioned in the post-mortem as a success: *That we all still like each other.* That should remain even if we're trying to hold to a product vision.

## Organizational dependency: Content Strategy, template leads

This is ultimately intertwined with Content Strategy, as the site would need to be ready for the 0.9 release.

And it's inherently intertwined with template leads, for what should be obvious reasons.

## Consequences

This proposal is well intended, but comes with at least one possible cost: It might change some of the more freewheeling sense of creation and identity important to some contributors, which often is cultivated by a purely bottom-up process. If that's you, I'd love for you to share reservations you have, so we can address them in the outset!

## Links and prior art

{This section is optional if you want to [link](https://example.com) to other resources.}


## Open questions

{This section is optional and is where you can list questions that won't be resolved by this RFC, including those raised in comments from community members.}


## Decisions deferred

* Specifics about the product direction
* Those involved in product leadership
* Specifics on processes with product leadership

## Feedback

{If you accept feedback from a community member, you will incorporate it into your RFC before it is accepted.
If you reject feedback, note that rejected feedback here before resolving the conversation.}


## Implementation checklist

If this proposal is accepted, the following tasks must be completed:

- [ ] Document this process for internal reference
- [ ] Draft and come to agreement to what should be in 1.0
    - [ ] Work with template leads to come to consensus on the roadmap
	- [ ] Decide on our acceptance criteria/definition of "done & confidently releasable"
	- [ ] Formulate the framework for the collected product
	- [ ] A timeline for template package execution for 0.9 & 1.0
	- [ ] A timeline for content strategy implementation in time for 0.9 & 1.0
	- [ ] A sense of what to have/leave in a "dev" channel, to be fully released post-1.0
- [ ] Set regular meetings or check-ins up with template leads and product lead (possibly as part of existing template leads meeting)


## Votes

Votes per our [decision process](https://thegooddocsproject.dev/decisions/):

Project steering committee (listed alphabetically by first name):

- Aaron Peters:
- Alyssa Rock:
- Ankita Tripathi:
- Bryan Klein:
- Cameron Shorter:
- Carrie Crowe:
- Erin McKean:
- Deanna Thompson:
- Felicity Brand:
- Gayathri Krishnaswamy:
- Morgan Craft:
- Nelson Guya:
- Ryan Macklin:
- Tina Lüdtke:


Community members who voted (non-binding):

- {Your name}: {Your vote}
