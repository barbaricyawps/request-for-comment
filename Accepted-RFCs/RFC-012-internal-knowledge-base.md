# RFC-012: Internal Knowledge Base
## Proposed by

Ryan Macklin ([@macklin](https://thegooddocs.slack.com/team/U01DYRWG43X))

Initially submitted: 24 Aug 2022

Last updated: 13 Dec 2022

## Current status

- [x] Draft
- [x] Under discussion
- [ ] Final comment and voting (until YYYY-MM-DD) {{Add date after selecting this status.}}
- [ ] Accepted
- [ ] Rejected
- [ ] Implemented
- [ ] Deferred
- [ ] Withdrawn

## Proposal overview

We need a central knowledge repository for contributors that:
* Isn't fragmented across different platforms or accounts
* Is easy for new contributors to access
* Has a sense of hierarchy
* Allows contributors to dump critical or needed info in a place that prevents them from becoming a bottleneck
* Has some sense of discipline/oversight to keep it from becoming disorganized or discordant

This is internal-facing material, meant to be consumed by members of the Good Docs project. Our template/product users don't need to know this stuff, but there's no reason to hide it. Therefore, we keep all information viewable to the public.

## Motivation

We don't have a good way to share information to use contributors, or a common places where information can live and grow. This was mentioned by multiple people during our recent retrospectives.

## Proposal

We could use a GitLab wiki or repo as a place where contributors can post material or comment on said material with questions, corrections, etc.

We'd have some guideline on structure, intent, etc. Each core initiative would have a space for their information, along with general information, community resources, etc.

The KB should be easily visible and searchable for new contributors, especially those who are new to GitLab. We want to avoid situtions where viewing the KB is cumbersome, such as Tina's concerns: "For example, I experienced that many of my Chronologue group members are pretty new to Github (let alone Gitlab), and switching/finding things in different places than their 'home' repo is pretty hard for them."

Ideally, our docs would make use of our templates—in fact, they would make for great use/test cases. However, we would allow for a "quickly written" status, in cases where information needs to be dumped from someone's mind, but the time/effort cost in polishing isn't currently possible.

We'd have a mechanism for checking the freshness of content, though we can defer the details of that until later. That may inculde owners or reviewers listed on individual articles or sections.

### Ideal time for release

We have events for new community members in late January. It'd be rad if we could have something stood up with basic info in place before then, so we can point it out as "it exists and is new" rather than "it's going to exist soon, sorry."

We don't need a lot to effectively launch this for new community members, just some foundation.

### Fail fast, fail forward

We can try a platform, see if it works, and if it doesn't we figure out a new solution (without needing a full RFC) and migrate info over. This is preferrable to debating the merits of various systems without diving into one.

Since this is a purely internal tool, we aren't creating difficulties with our wider userbase. And any difficulties we create within our org end up being typical growing pains and come with the notion that trying something different is to resolve existing pain points. (Just like how "have a solid, reliable place for info" addresses the current pain point.)

### Roles, responsibilities & existing working groups

This is a community-led sub-project, and doesn't require making a new working group. The various responsibilities would lie in:
* Tech team for technical implementation & maintainance (with RFC author working alongside for implementation)
* Community managers for thorny issues their team tackles in any medium: when there's some abuse, conflict, or other issues
* Individual working groups would maintain their own sections
* Any community member could contribute to help resolve any outdated info, reorganize as needed, etc.
  * That also covers "global" documents, as anyone has passion or interest to contribute

Using a [RACI framework](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example) to draft an initial understanding of roles & responsibilities:
* RACI stands for **R**esponsible, **A**ccountable, **C**onsulted, **I**nformed (using "-" for none), with each letter representing the level of task responsibility on a project

<table>
  <tr><td>Role→ <br> Task↓</td><th>RFC author</th><th>Tech team</th><th>Community managers</th><th>Working groups</th><th>General members</th></tr>
  <tr><th>Tech<sup style="color: navy">1</sup>—creation</th><td>A</td><td>R</td><td>I</td><td>I</td><td>-</td>
  <tr><th>Tech<sup style="color: navy">1</sup>—maintenance</th><td>-</td><td>R</td><td>C</td><td>C</td><td>-</td></tr>
  <tr><th>Access control</th><td>C</td><td>R</td><td>I</td><td>I</td><td>I<sup style="color: navy">3</sup></td></tr>
  <tr><th>Initial info population</th><td>R</td><td>C<sup style="color: navy">4</sup></td><td>C<sup style="color: navy">4</sup></td><td>R<sup style="color: navy">2</sup></td><td>I<sup style="color: navy">3</sup></td></tr>
  <tr><th>Meta-information<br />(how to use the KB)</th><td>R</td><td>C</td><td>C</td><td>I<sup style="color: navy">3</sup></td><td>I<sup style="color: navy">3</sup></td></tr>
  <tr><th>Long-term info management<br />(population, maintenance, deprecation...)</th><td>-</td><td>C<sup style="color: navy">5</sup></td><td>-</td><td>R</td><td>C<sup style="color: navy">4</sup></td></tr>
  <tr><th>Conflict resolution<br />(abuse, conflicts)</th><td>-</td><td>I<sup style="color: navy">6</sup></td><td>R</td><td>I<sup style="color: navy">6</sup></td><td>I<sup style="color: navy">6</sup></td></tr>
</table>

Nuance:
* <sup style="color: navy">1</sup>Using "tech" to mean wiki, Hugo sub-site, whatever we go with to start and as time goes on.
* <sup style="color: navy">2</sup>Responsible to a lesser extent, as relevant and able
* <sup style="color: navy">3</sup>"Informed" here meaning "they'll inherently see the information on the KB itself, with no extra effort to inform necessary"
* <sup style="color: navy">4</sup>"Consulted" here meaning "anticipated to contribute to the KB itself"
* <sup style="color: navy">5</sup>Only informed or consulted in the case where tech changes or migrations are foreseen or requested
* <sup style="color: navy">6</sup>"Informed" here meaning "expect messages in Slack"

### GitLab wiki: a.k.a. Do we already have this?
There's a wiki in GitLab, and [Aaron has release weight information there](https://gitlab.com/tgdp/governance/-/wikis/Guide-to-assigning-weight-scores-to-issues-and-epics-%28release-planning%29). Should we just go with that? Does that make sense for us as a group of groups?

Comment from Aaron: "In my mind the (a) wiki is the place for this type of information, as it's basically what they were designed for. The tricky part will be where it resides, and slightly related, if there's one for everything or one for each WG (as each repo has its own wiki I believe). I usually favor the "source of truth" approach, which implies we have one wiki to rule them all. Of course, at this point the IA and content strategy becomes super important..."

If we use a wiki, can we keep that wiki to being only for KB material? Ideally, we shouldn't have a wiki oversaturated with multiple purposes? Would it be difficult to use search functionality when looking for KB articles, due to non-KB stuff in the wiki also populating the search results?

### What about non-doc/external resources?

We'll have other resources for the project, such as a Google Slides template for making new presentations (on Ryan's winter to-do list) or future videos for learning git. We'll figure out the best way to handle such situations, but as far as how those situations would fit here, simple: there'd be external links as needed.

We don't want to external link everything, but where it's key, we do and that's okay. Those links should be clearly marked as external though.

## Consequences

We'd have a central place for information.

Said information though could become out of date without a contributor realizing it, due to the nature of knowledge bases and the bandwidth of contributors.

We also need to decide on the access control. Having new contributors being able to edit can be great for finding holes in the content, but could also be an issue if contributors put misunderstandings into our KB without others noticing right away.

Related, we need ways to roll back individual pages, to quickly resolve any potential abuse issues or just a passionate but ill-informed community member putting poor information in the KB.

Comment from Tina: "I see this as a blessing and curse as well. Having a stricter format that needs a PR before contributing would prevent these things, I guess."

This space would also be great for small procedural elements that should be documented, but don't need a full RFC for said documentation (such as the recent "break/restart" process the co-chairs initiated). That way such info isn't lost to the ether due to a lack of documentation procedure for such things.

## Links and prior art

{This section is optional if you want to [link](https://example.com) to other resources.}


## Open questions

{This section is optional and is where you can list questions that won't be resolved by this RFC, including those raised in comments from community members.}


## Decisions deferred

None listed, but we expect some tasks and decisions to be implemented post-initial launch, and that's okay!

## Feedback

{If you accept feedback from a community member, you will incorporate it into your RFC before it is accepted.
If you reject feedback, note that rejected feedback here before resolving the conversation.}

## Organizational dependencies

* The tech team would be involved in implementation
* Content strategy may have thoughts, even with this being internal-facing info

## Implementation checklist

If this proposal is accepted, the following tasks must be completed (though not necessarily all completed before initial launch):

- [ ] Governance list
  - [ ] Create issues for all these in GitLab
  - [ ] Sketch initial few posts necessary for contributors
- [ ] Organization list
  - [ ] Draft the core info on how to use & contribute, so we're all on the same page about it<sup style="color: navy">*</sup>
  - [ ] Ask working group leads to dump critical or infrastructure knowledge from their brains or disparate notes into the KB<sup style="color: navy">*</sup>
  - [ ] Scrape the existing GitHub repos and wikis for any project and process information that is relevant to capture in the KB*
  - [ ] Comb existing Google drives per working group for relevant info to move into the KB<sup style="color: navy">*</sup>
  - [ ] Draft initial intents/goals behind long-term elements (such as freshness goals & mechanisms)<sup style="color: navy">*</sup>
- [ ] Technical list
  - [ ] Set the site/wiki/whatever up
  - [ ] Create the initial structure/document tree<sup style="color: chocolate">+</sup>
  - [ ] Access control (a.k.a. who can edit/update)

Any marked with an asterisk (<sup style="color: navy">*</sup>) don't necessarily need to be completed before initial launch. Be great if they were, but in the vein of being nimble, they aren't requirements.

Any marked with a plus (<sup style="color: chocolate">+</sup>) are explicitly called out as "we'll do our best guess, and adjust over time," to make it clear we aren't trying to get it perfect right away.

## Votes

Project steering committee (listed alphabetically by first name):

- Aaron Peters: +1
- Alyssa Rock: +1
- Ankita Tripathi:
- Bryan Klein: +1
- Cameron Shorter: +1
- Carrie Crowe: +1
- Deanna Thompson: +1 
- Erin McKean:
- Felicity Brand: +1
- Gayathri Krishnaswamy: +1
- Morgan Craft:
- Nelson Guya:
- Ryan Macklin: +1
- Tina Lüdtke: +1